var email = document.getElementById("provide_email");

email.addEventListener("input", function (event) {
  if (email.validity.typeMismatch) {
    email.setCustomValidity("Please insert an email address!");
  } else {
    email.setCustomValidity("");
  }
});