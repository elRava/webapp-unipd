/*
 * Copyright 2019 University of Padua, Italy
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package it.unipd.dei.webapp;

import org.xml.sax.Attributes;
import org.xml.sax.helpers.DefaultHandler;

import javax.xml.parsers.SAXParser;
import javax.xml.parsers.SAXParserFactory;
import java.io.File;

/**
 * Reads an RSS feed by using DOM.
 *
 * @author Nicola Ferro  (ferro@dei.unipd.it)
 * @version 1.0
 * @since 1.0
 */
public class SaxReadRssFeed extends DefaultHandler {

    // the initial size of an array
    private static final int ARRAY_SIZE = 100;

    // the size increment of an array
    private static final int ARRAY_INCREMENT = 50;

    // the title of the feed
    private String feedTitle = null;

    // the description of the feed
    private String feedDescription = null;

    // the link to the feed
    private String feedLink = null;

    // array containing the title of the items
    private String[] itemTitle = new String[ARRAY_SIZE];

    // array containing the description of the items
    private String[] itemDescription = new String[ARRAY_SIZE];

    // array containing the publication date of the items
    private String[] itemPubDate = new String[ARRAY_SIZE];

    // array containing the link to the items
    private String[] itemLink = new String[ARRAY_SIZE];

    // the item currently processed
    private int currentItem = 0;

    // indicates whether we are reading an item or not
    private boolean isItem = false;

    // buffer to read characters
    private StringBuilder buffer = null;

    @Override
    public void startElement(String uri, String name, String qName, Attributes atts) {

        // when an element starts, allocate the character buffer
        buffer = new StringBuilder();

        // check whether that element is an item
        if ("item".equals(qName)) {
            isItem = true;
        }
    }

    @Override
    public void endElement(String uri, String name, String qName) {
        if ("item".equals(qName)) {
            // we reached the end of an item
            isItem = false;
            currentItem++;
        } else if ("title".equals(qName)) {
            // we reached the end of a title
            // determine whether it is under channel or under item and
            // store the contents in the appropriate variable
            if (isItem) {
                itemTitle = append(itemTitle, buffer.toString());
            } else {
                feedTitle = buffer.toString();
            }
        } else if ("description".equals(qName)) {
            // we reached the end of a description
            // determine whether it is under channel or under item and
            // store the contents in the appropriate variable
            if (isItem) {
                itemDescription = append(itemDescription, buffer.toString());
            } else {
                feedDescription = buffer.toString();
            }
        } else if ("link".equals(qName)) {
            // we reached the end of a link
            // determine whether it is under channel or under item and
            // store the contents in the appropriate variable
            if (isItem) {
                itemLink = append(itemLink, buffer.toString());
            } else {
                feedLink = buffer.toString();
            }
        } else if ("pubDate".equals(qName)) {
            // we reached the end of the publication date
            itemPubDate = append(itemPubDate, buffer.toString());
        } else {
            System.out.printf("Element %s unknown.%n", qName);
        }
    }

    @Override
    public void characters(char ch[], int start, int length) {
        // append characters from the current element to the buffer
        buffer.append(ch, start, length);
    }

    /**
     * Prints the results of the processing.
     */
    public void print() {

        String[] newArray;

        // adjust the array size to the number of items actually read
        newArray = new String[currentItem];
        System.arraycopy(itemTitle, 0, newArray, 0, currentItem);
        itemTitle = newArray;

        newArray = new String[currentItem];
        System.arraycopy(itemDescription, 0, newArray, 0, currentItem);
        itemDescription = newArray;

        newArray = new String[currentItem];
        System.arraycopy(itemLink, 0, newArray, 0, currentItem);
        itemLink = newArray;

        newArray = new String[currentItem];
        System.arraycopy(itemPubDate, 0, newArray, 0, currentItem);
        itemPubDate = newArray;

        System.out.printf("%n---------------------------------%n");

        System.out.printf("Feed title: %s%n", feedTitle);
        System.out.printf("Feed description: %s%n", feedDescription);
        System.out.printf("Link to the feed: %s%n", feedLink);
        System.out.printf("Items of the feed:%n");
        for (int i = 0; i < currentItem; i++) {
            System.out.printf(" - item title: %s%n", itemTitle[i]);
            System.out.printf(" - item description: %s%n", itemDescription[i]);
            System.out.printf(" - link to the item: %s%n", itemLink[i]);
            System.out.printf(" - item publication date: %s%n%n", itemPubDate[i]);
        }

    }

    /**
     * Appends an element to an array.
     *
     * If necessary it increases the size of the array.
     *
     * @param array the array to which the element has to be appended.
     * @param value the element to append.
     *
     * @return the array with the appended element.
     */
    private String[] append(String[] array, String value) {

        if (array.length == currentItem) {
            String[] newArray = new String[array.length + ARRAY_INCREMENT];
            System.arraycopy(array, 0, newArray, 0, array.length);
            array = newArray;
        }

        array[currentItem] = value;

        return array;
    }

    /**
     * Reads an RSS feed by using SAX.
     *
     * @param args args[0] the file to be read.
     *
     * @throws Exception if any error occurs while reading the file.
     */
    public static void main(String[] args) throws Exception {

        // start time of the processing
        long start;

        // end time of the processing
        long end;

        // btain an instance of the factory for creating SAX parsers
        SAXParserFactory factory = SAXParserFactory.newInstance();

        // obtain an instance of a SAX parsers
        SAXParser parser = factory.newSAXParser();

        // create a new SAX event handles, i.e. an instance of this class
        SaxReadRssFeed handler = new SaxReadRssFeed();

        // start processing the RSS feed
        start = System.currentTimeMillis();

        // parse the file
        parser.parse(new File(args[0]), handler);

        // end processing the RSS feed
        end = System.currentTimeMillis();

        // print the results of the processing
        handler.print();

        System.out.printf("Total time for processing the input file: %,d millisecs.%n", end - start);

    }

}