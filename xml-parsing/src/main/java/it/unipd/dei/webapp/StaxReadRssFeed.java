/*
 * Copyright 2019 University of Padua, Italy
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package it.unipd.dei.webapp;


import javax.xml.stream.XMLInputFactory;
import javax.xml.stream.XMLStreamConstants;
import javax.xml.stream.XMLStreamReader;
import java.io.File;
import java.io.FileInputStream​;

/**
 * Reads an RSS feed by using StAX.
 *
 * @author Nicola Ferro  (ferro@dei.unipd.it)
 * @version 1.0
 * @since 1.0
 */
public class StaxReadRssFeed {

    // the initial size of an array
    private static final int ARRAY_SIZE = 100;

    // the size increment of an array
    private static final int ARRAY_INCREMENT = 50;


    /**
     * Appends an element to an array.
     *
     * If necessary it increases the size of the array.
     *
     * @param array       the array to which the element has to be appended.
     * @param currentItem the total number of items processed so far.
     * @param value       the element to append.
     *
     * @return the array with the appended element.
     */
    private static String[] append(String[] array, int currentItem, String value) {

        if (array.length == currentItem) {
            String[] newArray = new String[array.length + ARRAY_INCREMENT];
            System.arraycopy(array, 0, newArray, 0, array.length);
            array = newArray;
        }

        array[currentItem] = value;

        return array;
    }


    /**
     * Reads an RSS feed by using StAX.
     *
     * @param args args[0] the file to be read.
     *
     * @throws Exception if any error occurs while reading the file.
     */
    public static void main(String[] args) throws Exception {

        // the title of the feed
        String feedTitle = null;

        // the description of the feed
        String feedDescription = null;

        // the link to the feed
        String feedLink = null;

        // the item currently processed
        int currentItem = 0;

        // array containing the title of the items
        String[] itemTitle = new String[ARRAY_SIZE];

        // array containing the description of the items
        String[] itemDescription = new String[ARRAY_SIZE];

        // array containing the publication date of the items
        String[] itemPubDate = new String[ARRAY_SIZE];

        // array containing the link to the items
        String[] itemLink = new String[ARRAY_SIZE];

        // indicates whether we are reading an item or not
        boolean isItem = false;

        // start time of the processing
        long start;

        // end time of the processing
        long end;

        // obtain an instance of the factory for creating StAX parsers
        XMLInputFactory xif = XMLInputFactory.newInstance();

        // obtain an instance of a StAX parser
        XMLStreamReader parser = xif.createXMLStreamReader(new FileInputStream​(new File(args[0])));

        // start processing the RSS feed
        start = System.currentTimeMillis();

        // while there are events in the stream
        while (parser.hasNext()) {

            // advance to the next event in the stream
            parser.next();

            // a new element is starting
            if (parser.getEventType() == XMLStreamConstants.START_ELEMENT) {

                if ("item".equals(parser.getLocalName())) {
                    // we reached the start of an item
                    isItem = true;
                } else if ("title".equals(parser.getLocalName())) {
                    // we reached the start of a title
                    // determine whether it is under channel or under item and
                    // store the contents in the appropriate variable
                    if (isItem) {
                        // move to the text node inside the element
                        parser.next();

                        itemTitle = append(itemTitle, currentItem, parser.getText());
                    } else {
                        // move to the text node inside the element
                        parser.next();

                        feedTitle = parser.getText();
                    }
                } else if ("description".equals(parser.getLocalName())) {
                    // we reached the end of a description
                    // determine whether it is under channel or under item and
                    // store the contents in the appropriate variable
                    if (isItem) {
                        // move to the text node inside the element
                        parser.next();

                        itemDescription = append(itemDescription, currentItem, parser.getText());
                    } else {
                        // move to the text node inside the element
                        parser.next();

                        feedDescription = parser.getText();
                    }
                } else if ("link".equals(parser.getLocalName())) {
                    // we reached the end of a link
                    // determine whether it is under channel or under item and
                    // store the contents in the appropriate variable
                    if (isItem) {
                        // move to the text node inside the element
                        parser.next();

                        itemLink = append(itemLink, currentItem, parser.getText());
                    } else {
                        // move to the text node inside the element
                        parser.next();

                        feedLink = parser.getText();
                    }
                } else if ("pubDate".equals(parser.getLocalName())) {
                    // we reached the end of the publication date

                    // move to the text node inside the element
                    parser.next();

                    itemPubDate = append(itemPubDate, currentItem, parser.getText());
                } else {
                    System.out.printf("Element %s unknown.%n", parser.getLocalName());
                }

            } else if (parser.getEventType() == XMLStreamConstants.END_ELEMENT && "item".equals(
                    parser.getLocalName())) {
                // we reached the end of an item
                isItem = false;

                currentItem++;
            }
        }

        // close the parser
        parser.close();

        // end processing the RSS feed
        end = System.currentTimeMillis();


        System.out.printf("%n---------------------------------%n");

        System.out.printf("Feed title: %s%n", feedTitle);
        System.out.printf("Feed description: %s%n", feedDescription);
        System.out.printf("Link to the feed: %s%n", feedLink);
        System.out.printf("Items of the feed:%n");
        for (int i = 0; i < currentItem; i++) {
            System.out.printf(" - item title: %s%n", itemTitle[i]);
            System.out.printf(" - item description: %s%n", itemDescription[i]);
            System.out.printf(" - link to the item: %s%n", itemLink[i]);
            System.out.printf(" - item publication date: %s%n%n", itemPubDate[i]);
        }

        System.out.printf("Total time for processing the input file: %,d millisecs.%n", end - start);


    }

}