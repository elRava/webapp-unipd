/*
 * Copyright 2019 University of Padua, Italy
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package it.unipd.dei.webapp;

import javax.xml.stream.XMLOutputFactory;
import javax.xml.stream.XMLStreamWriter;
import java.io.File;
import java.io.FileOutputStream;

/**
 * Writes an RSS feed by using StAX.
 *
 * @author Nicola Ferro  (ferro@dei.unipd.it)
 * @version 1.0
 * @since 1.0
 */
public class StaxWriteRssFeed {

    /**
     * Writes an RSS feed by using StAX.
     *
     * @param args args[0] the file to be read.
     *
     * @throws Exception if any error occurs while reading the file.
     */
    public static void main(String[] args) throws Exception {

        // the title of the feed
        String feedTitle = "Grid@CLEF News";

        // the description of the feed
        String feedDescription = "Events and updates about the Grid@CLEF track.";

        // the link to the feed
        String feedLink = "http://ims.dei.unipd.it/gridclef/";

        // total number of items in the feed
        int numItem = 2;

        // array containing the title of the items
        String[] itemTitle = new String[numItem];
        itemTitle[0] = "Terrier Support for Grid@CLEF";
        itemTitle[1] = "Registration to Grid@CLEF 2009 opens";

        // array containing the description of the items
        String[] itemDescription = new String[numItem];
        itemDescription[0] = "The Terrier open source information retrieval system will support the CIRCO framework.";
        itemDescription[1] = "Registration for Grid@CLEF 2009 opens today.";

        // array containing the publication date of the items
        String[] itemPubDate = new String[numItem];
        itemPubDate[0] = "Wed, 11 Feb 2009 15:41:49 GMT";
        itemPubDate[1] = "Wed, 4 Feb 2009 00:00:00 GMT";

        // array containing the link to the items
        String[] itemLink = new String[numItem];
        itemLink[0] = "http://ir.dcs.gla.ac.uk/terrier/issues/browse/TR-9";
        itemLink[1] = "http://www.clef-campaign.org/";

        // obtain a factory for creating XML writers
        XMLOutputFactory xof = XMLOutputFactory.newInstance();

        // the parser for writing the RSS feed
        XMLStreamWriter parser = xof.createXMLStreamWriter(new FileOutputStream​(new File(args[0])), "UTF-8");


        // emit the start document event
        parser.writeStartDocument();

        // emit the start element event for the rss element
        parser.writeStartElement("rss");
        parser.writeAttribute("version", "2.0");

        // emit the start element event for the channel element
        parser.writeStartElement("channel");

        // emit the start and end element events for the title element
        parser.writeStartElement("title");
        parser.writeCharacters(feedTitle);
        parser.writeEndElement();

        // emit the start and end element events for the description element
        parser.writeStartElement("description");
        parser.writeCharacters(feedDescription);
        parser.writeEndElement();

        // emit the start and end element events for the link element
        parser.writeStartElement("link");
        parser.writeCharacters(feedLink);
        parser.writeEndElement();


        // iterate over the items
        for (int i = 0; i < numItem; i++) {
            // emit the start element event for the item element
            parser.writeStartElement("item");

            // emit the start and end element events for the title element
            parser.writeStartElement("title");
            parser.writeCharacters(itemTitle[i]);
            parser.writeEndElement();

            // emit the start and end element events for the pubDate element
            parser.writeStartElement("pubDate");
            parser.writeCharacters(itemPubDate[i]);
            parser.writeEndElement();

            // emit the start and end element events for the link element
            parser.writeStartElement("link");
            parser.writeCharacters(itemLink[i]);
            parser.writeEndElement();

            // emit the start and end element events for the guid element
            parser.writeStartElement("guid");
            parser.writeAttribute("isPermaLink", "false");
            String guid = Long.toString((long) Integer.MAX_VALUE + itemTitle[i].hashCode());
            parser.writeCharacters(guid);
            parser.writeEndElement();

            // emit the start and end element events for the description element
            parser.writeStartElement("description");
            parser.writeCharacters(itemDescription[i]);
            parser.writeEndElement();

            // emit the end element event for the item element
            parser.writeEndElement();
        }

        // emit the end element event for the channel element
        parser.writeEndElement();

        // emit the end element event for the rss element
        parser.writeEndElement();

        // emit the end document event
        parser.writeEndDocument();

        // close the parser and flush to disk
        parser.close();

        System.out.printf("RSS feed successfully written to file: %s%n", args[0]);

    }
}