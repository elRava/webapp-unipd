/*
 * Copyright 2019 University of Padua, Italy
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package it.unipd.dei.webapp;

import org.xml.sax.helpers.AttributesImpl;

import javax.xml.transform.OutputKeys;
import javax.xml.transform.Transformer;
import javax.xml.transform.sax.SAXTransformerFactory;
import javax.xml.transform.sax.TransformerHandler;
import javax.xml.transform.stream.StreamResult;
import java.io.File;

/**
 * Writes an RSS feed by using SAX.
 *
 * @author Nicola Ferro  (ferro@dei.unipd.it)
 * @version 1.0
 * @since 1.0
 */
public class SaxWriteRssFeed {

    /**
     * Writes an RSS feed by using SAX.
     *
     * @param args args[0] the file to be read.
     *
     * @throws Exception if any error occurs while reading the file.
     */
    public static void main(String[] args) throws Exception {

        // the title of the feed
        String feedTitle = "Grid@CLEF News";

        // the description of the feed
        String feedDescription = "Events and updates about the Grid@CLEF track.";

        // the link to the feed
        String feedLink = "http://ims.dei.unipd.it/gridclef/";

        // total number of items in the feed
        int numItem = 2;

        // array containing the title of the items
        String[] itemTitle = new String[numItem];
        itemTitle[0] = "Terrier Support for Grid@CLEF";
        itemTitle[1] = "Registration to Grid@CLEF 2009 opens";

        // array containing the description of the items
        String[] itemDescription = new String[numItem];
        itemDescription[0] = "The Terrier open source information retrieval system will support the CIRCO framework.";
        itemDescription[1] = "Registration for Grid@CLEF 2009 opens today.";

        // array containing the publication date of the items
        String[] itemPubDate = new String[numItem];
        itemPubDate[0] = "Wed, 11 Feb 2009 15:41:49 GMT";
        itemPubDate[1] = "Wed, 4 Feb 2009 00:00:00 GMT";

        // array containing the link to the items
        String[] itemLink = new String[numItem];
        itemLink[0] = "http://ir.dcs.gla.ac.uk/terrier/issues/browse/TR-9";
        itemLink[1] = "http://www.clef-campaign.org/";

        // obtain a factory and an instance of a transformer, i.e. a class concerned with serializing XML
        SAXTransformerFactory tf = (SAXTransformerFactory) SAXTransformerFactory.newInstance();

        TransformerHandler handler = tf.newTransformerHandler();
        handler.setResult(new StreamResult(new File(args[0])));

        Transformer t = handler.getTransformer();
        t.setOutputProperty(OutputKeys.INDENT, "yes");
        t.setOutputProperty(OutputKeys.METHOD, "xml");
        t.setOutputProperty(OutputKeys.ENCODING, "UTF-8");

        AttributesImpl atts = new AttributesImpl();

        // emit the start document event
        handler.startDocument();

        // emit the start element event for the rss element
        atts.addAttribute("", "", "version", "CDATA", "2.0");
        handler.startElement("", "", "rss", atts);
        atts.clear();

        // emit the start element event for the channel element
        handler.startElement("", "", "channel", atts);

        // emit the start and end element events for the title element
        handler.startElement("", "", "title", atts);
        handler.characters(feedTitle.toCharArray(), 0, feedTitle.length());
        handler.endElement("", "", "title");

        // emit the start and end element events for the description element
        handler.startElement("", "", "description", atts);
        handler.characters(feedDescription.toCharArray(), 0, feedDescription.length());
        handler.endElement("", "", "description");

        // emit the start and end element events for the link element
        handler.startElement("", "", "link", atts);
        handler.characters(feedLink.toCharArray(), 0, feedLink.length());
        handler.endElement("", "", "link");

        // iterate over the items
        for (int i = 0; i < numItem; i++) {

            // emit the start element event for the item element
            handler.startElement("", "", "item", atts);

            // emit the start and end element events for the title element
            handler.startElement("", "", "title", atts);
            handler.characters(itemTitle[i].toCharArray(), 0, itemTitle[i].length());
            handler.endElement("", "", "title");

            // emit the start and end element events for the pubDate element
            handler.startElement("", "", "pubDate", atts);
            handler.characters(itemPubDate[i].toCharArray(), 0, itemPubDate[i].length());
            handler.endElement("", "", "pubDate");

            // emit the start and end element events for the link element
            handler.startElement("", "", "link", atts);
            handler.characters(itemLink[i].toCharArray(), 0, itemLink[i].length());
            handler.endElement("", "", "link");

            // emit the start and end element events for the guid element
            atts.clear();
            atts.addAttribute("", "", "isPermaLink", "CDATA", "false");
            handler.startElement("", "", "guid", atts);
            String guid = Long.toString((long) Integer.MAX_VALUE + itemTitle[i].hashCode());
            handler.characters(guid.toCharArray(), 0, guid.length());
            handler.endElement("", "", "guid");

            // emit the start and end element events for the description element
            handler.startElement("", "", "description", atts);
            handler.characters(itemDescription[i].toCharArray(), 0, itemDescription[i].length());
            handler.endElement("", "", "description");

            // emit the end element event for the item element
            handler.endElement("", "", "item");

        }

        // emit the end element event for the channel element
        handler.endElement("", "", "channel");

        // emit the end element event for the rss element
        handler.endElement("", "", "rss");

        // emit the end document event
        handler.endDocument();

        System.out.printf("RSS feed successfully written to file: %s%n", args[0]);

    }
}