/*
 * Copyright 2018 University of Padua, Italy
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package it.unipd.dei.webapp;

import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.net.URL;

import javax.net.ssl.HttpsURLConnection;

import org.json.simple.JSONArray;
import org.json.simple.JSONObject;
import org.json.simple.JSONValue;

/**
 * Translates "Hello, world" in the requested language.
 * 
 * @author Nicola Ferro  (ferro@dei.unipd.it)
 * @version 1.0
 * @since 1.0
 */
public class TranslateHelloWorld {

	/**
	 * The URL of the Web API for translation
	 */
	private static final String URL = "https://translate.yandex.net/api/v1.5/tr.json/translate?key=%s&lang=en-%s&text=Hello+world";

	/**
	 * Translates the string "Hello world" into the requested language.
	 * 
	 * @param args
	 *            command line arguments. {@code args[0]} is the ISO 639-1 code
	 *            of the target language; {@code args[1]} is Yandex API key.
	 * @throws Exception
	 *             if something goes wrong during the translation.
	 */
	public static void main(String[] args) throws Exception {

		// get the target language or use Italian as default one
		final String lang = (args.length > 0 ? args[0] : "it");

		// get the API key or use Italian a default one
		final String key = (args.length > 1 ? args[1]
				: "trnsl.1.1.XXXX");

		// create the URL for the connection
		URL url = new URL(String.format(URL, key, lang));

		// open the connection to the Web server and set a GET request
		final HttpsURLConnection c = (HttpsURLConnection) url.openConnection();
		c.setRequestMethod("GET");

		// read the response from the Web API and parse it into a JSON object
		final JSONObject json = (JSONObject) JSONValue
				.parse(new BufferedReader(new InputStreamReader(c
						.getInputStream())));

		// extract the array of strings contained in the "text" field and get
		// the first string
		final String translation = (String) ((JSONArray) json.get("text"))
				.get(0);

		// extract the value of the "text" field and print it to the screen
		System.out.printf("%n%s%n", translation);

		// close the connection
		c.disconnect();
	}

}
