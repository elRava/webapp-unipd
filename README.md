# Web Applications (webapp)

This directory contains the source code distribution complementing the lectures.

Web Applications lectures are held at:

* Master Degree in ICT for Internet and Multimedia
* Master Degree in Computer Engineering

of the Department of Information Engineering, University of Padua, Italy

Copyright and license information can be found in the file LICENSE. 
Additional information can be found in the file NOTICE.
